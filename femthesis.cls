%----------------------------------------
% created by Rodolfo Jordao in 24/01/2017,
% based on a given thesis template made by Marcos Mendes, FEM, 2016
% in order to remove bloat and modernize packages
%
% Currently being maintained by Ricardo Bonna
%
%            MAIN CONTRIBUTORS:
% Wendell F. S. Diniz <wendellcien@gmail.com>
% Marcos Mendes
% Rodolfo Jordao <jor.rodolfo@gmail.com>
% Ricardo Bonna <ricardobonna@gmail.com>
%----------------------------------------

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{femthesis}[01/02/2017 Unicamp FEM Thesis Class]

%----------------------------------------
% based on the report class, for wider availability
%----------------------------------------

\LoadClass[a4paper, 12pt]{report}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{report}}
\DeclareOption{englishthesis}{\def\isEnglish{1}}
\DeclareOption{phd}{\def\isPhD{1}}
\DeclareOption{doutorado}{\def\isPhD{1}}
\ProcessOptions\relax


%----------------------------------------
% required packages for fonts
% remember to consider engines as luatex
%----------------------------------------

\RequirePackage{ifluatex}
\ifluatex
	\RequirePackage{fontspec}
\else
	\RequirePackage[utf8]{inputenc}
\fi

%----------------------------------------
% required packages for image manipulation
%----------------------------------------

\RequirePackage{graphicx}

%----------------------------------------
% Optional package for better PDF support. Uncomment if you want to use it
%----------------------------------------

 \RequirePackage[
  	colorlinks=true, linkcolor=black, urlcolor=blue, citecolor=blue
 ]{hyperref}

%----------------------------------------
% localization and language
%----------------------------------------

\ifdefined\isEnglish
\else
  \RequirePackage[brazil]{babel}
\fi
% \RequirePackage{csquotes}

%----------------------------------------
% sane formatting, keeping to ABNT demands
%----------------------------------------

\RequirePackage[top=3cm, bottom=2cm, left=3cm, right=2cm]{geometry}
\RequirePackage[onehalfspacing]{setspace}
\RequirePackage{mathptmx}
\RequirePackage{natbib}
\RequirePackage{fancyhdr}
\fancypagestyle{plain}{
  \fancyhf{}
  \fancyhf[FLO]{\Author}
  \fancyhf[FRE]{\Author}
  \fancyhf[HRO]{\thepage}
  \fancyhf[HLE]{\thepage}
  \renewcommand{\headrulewidth}{0.0pt}
  \renewcommand{\footrulewidth}{0.3pt}
}
\fancypagestyle{main}{
  \fancyhf{}
  \fancyhf[FLO]{\Author}
  \fancyhf[FRE]{\Author}
  \fancyhf[HRO]{\thepage}
  \fancyhf[HLE]{\thepage}
  \fancyhf[HC]{\leftmark}
  \renewcommand{\headrulewidth}{0.3pt}
  \renewcommand{\footrulewidth}{0.3pt}
}
\RequirePackage{titlesec}
\titleformat{\chapter}
  {\Large\bfseries}
  {\thechapter}
  {.5em}
  {\MakeUppercase}
\titleformat{name=\chapter, numberless}
  {\Large\bfseries}
  {}
  {.5em}
  {}
\titleformat{\section}[block]
  {\Large\bfseries}
  {\thesection}
  {.5em}
  {}
\titleformat{\subsection}[block]
  {\large\bfseries}
  {\thesubsection}
  {.5em}
  {}

% for some reson, latex seems to put 0.5 baselineskip before each section,
% so we act accordingly.

\titlespacing*{\chapter}{0pt}{1.5\baselineskip}{2\baselineskip}
\titlespacing*{\section}{0pt}{1.5\baselineskip}{2\baselineskip}
\titlespacing*{\subsection}{0pt}{1.5\baselineskip}{2\baselineskip}

% hack to get double paging numberation right
\makeatletter
\def
\cleardoublepage{%
\clearpage
  \ifodd\c@page
  \else
    \hbox{}
    \thispagestyle{plain}
    \newpage
  \fi
  }%
\makeatother

%----------------------------------------
% package to set caption style
%----------------------------------------

\RequirePackage{caption}
\captionsetup{labelfont=bf, format=hang, labelsep=period, font=onehalfspacing}

%----------------------------------------
% packages to help generation
%----------------------------------------

% \RequirePackage{datetime}

%----------------------------------------
% packages for TOC
%----------------------------------------

% \RequirePackage{tocloft}
% \tocloftpagestyle{empty}

%----------------------------------------
% metadata auxiliary functions
%----------------------------------------

\renewcommand{\title}[1]{
  \def\Title{#1}
}
\newcommand{\titulo}[1]{
  \def\Titulo{#1}
}
\renewcommand{\author}[2][m]{
  \def\Author{#2}
  \ifx f#1 \def\fAuthor{} \fi
}
\newcommand{\autor}[2][m]{
  \author[#1]{#2}
}
\newcommand{\advisor}[2][m]{
  \def\Advisor{#2}
  \ifx f#1 \def\fAdvisor{} \fi
}
\newcommand{\orientador}[2][m]{
  \advisor[#1]{#2}
}
\newcommand{\coadvisor}[2][m]{
  \def\Coadvisor{#2}
  \ifx f#1 \def\fCoadvisor{} \fi
}
\newcommand{\coorientador}[2][m]{
  \coadvisor[#1]{#2}
}
\newcommand{\dept}[1]{
  \def\Dept{#1}
}
\newcommand{\type}[1]{
  \def\Type{#1}
}

\renewcommand{\date}[3]{
  \def\Day{#1}
  \def\Month{#2}
  \def\Year{#3}
}

\newcommand{\data}[3]{
  \date{#1}{#2}{#3}
}
\newcommand{\department}[1]{
  \def\Department{#1}
}
\newcommand{\departamento}[1]{
  \department{#1}
}

\newcommand{\firstprof}[2]{
  \def\FirstProf{#1}
  \def\FirstProfInstitution{#2}
}

\newcommand{\secondprof}[2]{
  \def\SecondProf{#1}
  \def\SecondProfInstitution{#2}
}

\newcommand{\thirdprof}[2]{
  \def\ThirdProf{#1}
  \def\ThirdProfInstitution{#2}
}

\newcommand{\fourthprof}[2]{
  \def\FourthProf{#1}
  \def\FourthProfInstitution{#2}
}

\newcommand{\fifthprof}[2]{
  \def\FifthProf{#1}
  \def\FifthProfInstitution{#2}
}

%----------------------------------------
% metadata auxiliary printring functions
%----------------------------------------

\newcommand{\printauthor}{
	\ifdefined\Author
		\Author
	\else
		\ifdefined\isEnglish
			Author Not defined
		\else
			Autor N\~{a}o Definido
		\fi
	\fi
}

\newcommand{\printautor}{
	\printauthor
}

\newcommand{\printauthorpretty}{
	\begin{center}
		{\bf \large \printauthor}
	\end{center}
}

\newcommand{\printadvisor}{
	\ifdefined\Advisor
		\Advisor
	\else
		\ifdefined\isEnglish
			Advisor Not defined
		\else
			Orientador N\~{a}o Definido
		\fi
	\fi
}

\newcommand{\printorientador}{
	\printadvisor
}

\newcommand{\printadvisorpretty}{
	\printadvisor
}

\newcommand{\printcoadvisor}{
	\ifdefined\Coadvisor
		\Coadvisor
	\else
		\ifdefined\isEnglish
			Coadvisor Not defined
		\else
			Coorientador N\~{a}o Definido
		\fi
	\fi
}

\newcommand{\printcoorientador}{
	\printcoadvisor
}

\newcommand{\printtitle}{
	\begin{center}
		\ifdefined\Title
			\linespread{1.8}
			\selectfont
			{\bf \LARGE \Title}
		\else
			{\em Title Not Defined}
		\fi
	\end{center}
}

\newcommand{\printtitulo}{
	\begin{center}
		\ifdefined\Titulo
			\linespread{1.8}
			\selectfont
			{\bf \LARGE \Titulo}
		\else
			{\em Título N\~{a}o definido}
		\fi
	\end{center}
}

\newcommand{\printyear}{
	\ifdefined\Year
		\Year
	\else
		\ifdefined\isEnglish
			Year not defined.
		\else
			Ano n\~{a}o definido.
		\fi
	\fi
}

\newcommand{\printmonth}{
	\ifdefined\Month
		\Month
	\else
		\ifdefined\isEnglish
			Month not defined.
		\else
			M\^{e}s n\~{a}o definido.
		\fi
	\fi
}

\newcommand{\printday}{
	\ifdefined\Day
		\Day
	\else
		\ifdefined\isEnglish
			Day not defined.
		\else
			Dia n\~{a}o definido.
		\fi
	\fi
}

\newcommand{\printdate}{
	\printday{} de\printmonth{} de\printyear{}
}

%----------------------------------------
% make commands for automatic pages generation
%----------------------------------------

\newcommand{\makefrontmatter}{
	%\pagenumbering{gobble}
	\pagestyle{empty}
}

\newcommand{\makemainmatter}{
	%\pagenumbering{arabic}
	\pagestyle{main}
  \renewcommand{\baselinestretch}{1.5}
}

\newcommand{\maketitlepage}{
	\begin{minipage}{0.1\textwidth}
		\centering
		\includegraphics[width=\textwidth]{unicamp}
	\end{minipage}
	\begin{minipage}{0.8\textwidth}
		\centering
		\linespread{1.5}
		\selectfont
		{{\large \bf \ifdefined\City \City \else UNIVERSIDADE ESTADUAL DE CAMPINAS \fi}\\
                  \ifdefined\Department \Department \else FACULDADE DE ENGENHARIA MEC\^{A}NICA \fi}
	\end{minipage}
	\begin{minipage}{0.1\textwidth}
		\centering
	\end{minipage}

	\vfill

	\printauthorpretty

	\vfill

	\ifdefined\isEnglish
		\printtitle

		\vfill
	\fi

	\printtitulo

	\vfill

	\begin{center}
		CAMPINAS \\ \printyear{}
	\end{center}

	\clearpage
}

\newcommand{\makesignaturepage}[4]{
	\makesignaturepagecore{#1}{#2}{#3}{#4}

	\clearpage
}

\newcommand{\makesignaturepagecore}[4]{

  \def\Preambulo{\singlespacing \ifdefined\isPhD Tese \else Disserta\c{c}\~{a}o
    \fi apresentada \`{a} Faculdade de Engenharia Mec\^{a}nica da Universidade
    Estadual de Campinas como parte dos requisitos exigidos para
    obten\c{c}\~{a}o do t\'{i}tulo de \ifdefined\isPhD \ifdefined\fAuthor
    Doutora \else Doutor \fi \else Mestre \fi em #1, na \'{A}rea de #2.}

  \def\Preamble{\singlespacing \ifdefined\isPhD Thesis \else Dissertation \fi
    presented to the School of Mechanical Engineering of the University of
    Campinas in partial fulfillment of the requirements for the degree of
    \ifdefined\isPhD Doctor \else Master \fi in #3, in the area of #4.}

	\printauthorpretty

	\ifdefined\isEnglish
		\vfill

		\printtitle
	\fi

	\vfill

	\printtitulo

	\vfill

		\ifdefined\isEnglish
			\begin{minipage}{0.49\textwidth}
				\hfill
			\end{minipage}
			\begin{minipage}{0.49\textwidth}
				\ifdefined\Preamble
					\Preamble
				\else
					{\em Preamble not defined}
				\fi
			\end{minipage}

			\vfill
		\fi

		\begin{minipage}{0.49\textwidth}
			\hfill
		\end{minipage}
		\begin{minipage}{0.49\textwidth}
			\ifdefined\Preambulo
				\Preambulo
			\else
				{\em Preâmbulo n\~{a}o definido}
			\fi
		\end{minipage}

	\vfill

	\noindent \ifdefined\fAdvisor Orientadora\else Orientador\fi: \printadvisor \\
	\ifdefined\Coadvisor
		\ifdefined\fCoadvisor Coorientadora\else Coorientador\fi: \printcoadvisor
	\fi

	\vfill

	\noindent
	\begin{minipage}{0.5\textwidth}
          \footnotesize \singlespacing
          ESTE EXEMPLAR CORRESPONDE \`{A} VERS\~{A}O FINAL DA \ifdefined\isPhD
          TESE \else DISSERTA\c{C}\~{A}O \fi DEFENDIDA \ifdefined\fAuthor PELA
          ALUNA \else PELO ALUNO \fi \MakeUppercase\printauthor, E ORIENTADA
          \ifdefined\fAdvisor PELA \else PELO \fi \MakeUppercase\printadvisor.
          \vspace{3mm}
	\end{minipage}
	\begin{minipage}{0.4\textwidth}
		\hfill
	\end{minipage}


	\vfill

	\begin{center}
		CAMPINAS \\ \printyear{}
	\end{center}

}

\newcommand{\makecatalogpage}[1]{
	\def\Catalog{#1}
	\ifx\Catalog\empty
		\begin{center}
                  {\large FICHA CATALOGR\'{A}FIA ELABORADA PELA \\ BIBLIOTECA DE
                    \'{A}REA DE ENGENHARIA E ARQUITETURA - BAE - UNICAMP} #1
		\end{center}

		\vfill

		{\Huge A SER PREENCHIDO POR PDF DA BAE}

		\vfill

		{\Huge TO BE FILLED WITH A PDF FROM BAE}

		\vfill
	\else
		\Catalog
	\fi

	\clearpage
}

\newcommand{\makeapprovalpage}{
  \begin{samepage}
	\begin{center}
		\linespread{1.3}
		\selectfont
		{\bf \large UNIVERSIDADE ESTADUAL DE CAMPINAS\\
		FACULDADE DE ENGENHARIA MEC\^{A}NICA\\
		COMISS\~{A}O DE P\'{O}S-GRADUA\c{C}\~{A}O EM ENGENHARIA MEC\^{A}NICA\\
		\ifdefined\Dept\MakeUppercase\Dept\else Dept Undefined\fi}
	\end{center}

	\vfill

	\begin{center}
          {\bf \ifdefined\isPhD TESE DE DOUTORADO\else DISSERTA\c{C}\~{A}O DE
            MESTRADO ACAD\^{E}MICO\fi}
	\end{center}

	\vfill

	\ifdefined\isEnglish
		\printtitle

		\vfill
	\fi

	\printtitulo
        
        \vspace{3ex}
	\vfill

	\noindent \ifdefined\fAuthor Autora\else Autor\fi: \printauthor \\
	\noindent \ifdefined\fAdvisor Orientadora\else Orientador\fi: \printadvisor \\
	\ifdefined\Coadvisor
		\ifdefined\fCoadvisor Coorientadora\else Coorientador\fi: \printcoadvisor
	\fi

        \vspace{1.5ex}

	\noindent A banca examinadora composta pelos membros abaixo aprovou esta
        \ifdefined\isPhD tese\else disserta\c{c}\~{a}o\fi:

        \ifdefined\isPhD \vspace{2ex} \else \vspace{4ex} \fi
        

	\noindent {\bf \ifdefined\FirstProf \FirstProf \else \fi}\\
	\ifdefined\FirstProfInstitution {\bf \FirstProfInstitution} \else\fi
	%Instituição

	\ifdefined\isPhD \vspace{2ex} \else \vspace{4ex} \fi

	\noindent {\bf \ifdefined\SecondProf \SecondProf\else \fi}\\
	\ifdefined\SecondProfInstitution {\bf \SecondProfInstitution} \else\fi
	%Instituição

	\ifdefined\isPhD \vspace{2ex} \else \vspace{4ex} \fi

	\noindent {\bf \ifdefined\ThirdProf \ThirdProf\else \fi}\\
	\ifdefined\ThirdProfInstitution {\bf \ThirdProfInstitution} \else\fi
	%Instituição

  \ifdefined\isPhD

	\vspace{2ex}

  	\noindent {\bf \ifdefined\FourthProf \FourthProf\else \fi}\\
  	\ifdefined\FourthProfInstitution {\bf \FourthProfInstitution} \else\fi
  	%Instituição

  	\vspace{2ex}

  	\noindent {\bf \ifdefined\FifthProf \FifthProf\else \fi}\\
  	\ifdefined\FifthProfInstitution {\bf \FifthProfInstitution} \else\fi
  	%Instituição

  \fi
  	\ifdefined\isPhD \vspace{2ex} \else \vspace{4ex} \fi
	\vfill

	\noindent A Ata da defesa com as respectivas assinaturas dos membros
        encontra-se no processo de vida acad\^{e}mica \ifdefined\fAuthor da
        aluna\else do aluno\fi.

	\vfill

	\begin{flushright}
		Campinas,\printdate{}
	\end{flushright}
  \end{samepage}
	\clearpage

}

\newcommand{\makededicatorypage}[1]{
	\hfill
	\vfill

	\begin{center}
		{\large \bf \ifdefined\isEnglish Dedicatory\else Dedicat\'{o}ria\fi}
	\end{center}

	\vspace{2ex}

	\begin{center}
		#1
	\end{center}

	\vfill

	\clearpage
}

\newcommand{\makethankspage}[1]{
	\hfill
	\vfill

	\begin{center}
		{\bf \large \ifdefined\isEnglish Acknowledgements\else Agradecimentos\fi}
	\end{center}

	\vspace{2ex}

	\begin{center}
		#1
	\end{center}

	\vfill

	\clearpage
}

\newcommand{\makeabstractpage}[2]{
	\hfill
	\vfill

	\begin{center}
		{\bf \Large Abstract}
	\end{center}

	\vspace{2ex}

	#1

	\vspace{3ex}

	{\em Keywords}: #2

	\vfill

	\clearpage
}

\newcommand{\makeresumopage}[2]{
	\hfill
	\vfill

	\begin{center}
		{\bf \Large Resumo}
	\end{center}

	\vspace{2ex}

	#1

	\vspace{3ex}

	{\em Palavras-chave}: #2

	\vfill

	\clearpage
}

\newcommand{\makeabstractresumopage}[4]{
	\hfill
	\vfill

	\begin{center}
		{\bf \Large Abstract}
	\end{center}

	\vspace{2ex}

	#1

	\vspace{3ex}

	\noindent {\em Keywords}: #2

	\vfill

	\begin{center}
		{\bf \Large Resumo}
	\end{center}

	\vspace{2ex}

	#3

	\vspace{3ex}

	\noindent {\em Palavras-chave}: #4

	\vfill

	\clearpage

}

\newcommand{\makelistofalgorithms}{
	\listofalgorithms
	%\clearpage
	\thispagestyle{empty}
}

\newcommand{\makelistoffigures}{
	\listoffigures
	%\clearpage
	\thispagestyle{empty}
}

\newcommand{\makelistoftables}{
	\listoftables
	%\clearpage
	\thispagestyle{empty}
}

\newcommand{\maketableofcontents}{
	\tableofcontents
	\thispagestyle{empty}
	%\clearpage
}
