# LaTeX-Makefile
#
# Author: Ingo Sander (adapted by Denis Loubach)
#
# Date: 20130420
#
# Description:
#
# The Makefile assumes that original figures are placed in the
# directory ./figs_orig. During execution of the makefile,
# LaTeX-compatible figures are generated in the directory ./figs. The
# name of the main-file has to be explicitly been given in the
# Makefile. LaTeX-files that shall be included shall be placed in the
# ./include directory. Currently the following figure-formats are
# supported: pdf, eps, tex, svg, jpg, png, fig.
#
# The following commands are supported:
#
# make
# make view
# make clean

MAIN            = example

FIGS_ORIG_DIR   = ./figs_orig
FIGS_DIR        = ./figs
LATEXDIR        = ./include/
BIBDIR	        = ./
HASKELLDIR      = ./src

PDF_FIGS	 = $(patsubst $(FIGS_ORIG_DIR)/%.pdf,$(FIGS_DIR)/%.pdf,$(wildcard $(FIGS_ORIG_DIR)/*.pdf))
EPS_FIGS	 = $(patsubst $(FIGS_ORIG_DIR)/%.eps,$(FIGS_DIR)/%.pdf,$(wildcard $(FIGS_ORIG_DIR)/*.eps))
TIKZ_FIGS = $(patsubst $(FIGS_ORIG_DIR)%.tex,$(FIGS_DIR)%.pdf,$(wildcard $(FIGS_ORIG_DIR)/*.tex))
SVG_FIGS	 = $(patsubst $(FIGS_ORIG_DIR)%.svg,$(FIGS_DIR)%.pdf,$(wildcard $(FIGS_ORIG_DIR)/*.svg))
JPG_FIGS	 = $(patsubst $(FIGS_ORIG_DIR)/%.jpg,$(FIGS_DIR)/%.jpg,$(wildcard $(FIGS_ORIG_DIR)/*.jpg))
PNG_FIGS	 = $(patsubst $(FIGS_ORIG_DIR)/%.png,$(FIGS_DIR)/%.png,$(wildcard $(FIGS_ORIG_DIR)/*.png))
XFIG_FIGS = $(patsubst $(FIGS_ORIG_DIR)/%.fig,$(FIGS_DIR)/%.pdf_t,$(wildcard $(FIGS_ORIG_DIR)/*.fig))

BIBFILES     = $(wildcard $(BIBDIR)/*.bib)
LATEXFILES   = $(wildcard $(LATEXDIR)/*.tex)
TEXFILES     = $(wildcard ./*.tex)
STYLEFILES   = $(wildcard ./*.sty)
HASKELLFILES = $(wildcard $(HASKELLDIR)/*.hs)

all: $(MAIN).pdf

view: $(MAIN).pdf
	evince $(MAIN).pdf &

$(MAIN).pdf : $(MAIN).tex $(EPS_FIGS) $(PDF_FIGS) $(XFIG_FIGS) $(SVG_FIGS) \
              $(JPG_FIGS) $(PNG_FIGS) $(LATEXFILES) $(TIKZ_FIGS) $(TEXFILES) \
              $(MAIN).bbl Makefile $(STYLEFILES) $(BIBFILES) $(HASKELLFILES)
	pdflatex --shell-escape $(MAIN)
	bibtex $(MAIN)
	pdflatex $(MAIN)
	@while ( grep "Rerun to get cross-references" 	\
	$(MAIN).log > /dev/null ); do		\
	        echo '** Re-running LaTeX **';		\
	        pdflatex $(MAIN);				\
	done

clean:
	rm -f ./$(FIGS_DIR)/*.pdf
	rm -f ./*.aux
	rm -f ./*.out
	rm -f ./*.tex~
	rm -f ./*.log
	rm -f ./*.ps
	rm -f ./*.dvi
	rm -f ./*.blg
	rm -f ./*.bbl
	rm -f ./*.tmp
	rm -f ./*~
	rm -f ./*.thm
	rm -f ./*.toc
	rm -f ./*.lo*
	rm -f ./comment.cut
	rm -f ./*.pdf
	rm -f ./*.out
	rm -f ./#*
	rm -f ./*.gz
	rm -f ./include/*.tex~
	rm -f ./include/*.out
	rm -f ./include/*.log


# Rules for original PDF figures
$(PDF_FIGS) : $(FIGS_DIR)/%.pdf: $(FIGS_ORIG_DIR)/%.pdf
	cp $< $@

# Rules for original JPG figures
$(JPG_FIGS) : $(FIGS_DIR)/%.jpg: $(FIGS_ORIG_DIR)/%.jpg
	cp $< $@

# Rules for original PNG figures
$(PNG_FIGS) : $(FIGS_DIR)/%.png: $(FIGS_ORIG_DIR)/%.png
	cp $< $@

# Rules for original EPS figures
GS_OPTS:= -dPDFX
$(EPS_FIGS) : $(FIGS_DIR)/%.pdf : $(FIGS_ORIG_DIR)/%.eps
        #Creates .pdf files from .esp files
	a2ping --gsextra='$(GS_OPTS)' --outfile=$@  $(<)

# Rules for Tikz and LaTeX figures
$(TIKZ_FIGS): $(FIGS_DIR)/%.pdf: $(FIGS_ORIG_DIR)/%.tex
	echo $(@F)
	mkdir -p $(FIGS_DIR)
	cd $(FIGS_ORIG_DIR); TEXINPUTS=:../ pdflatex $(*F);
	mv $(FIGS_ORIG_DIR)/$(@F) $@
	rm $(FIGS_ORIG_DIR)/*.log
	rm $(FIGS_ORIG_DIR)/*.aux

# Rules for SVG files
$(SVG_FIGS): $(FIGS_DIR)/%.pdf : $(FIGS_ORIG_DIR)/%.svg
	echo $<
	inkscape -z -D --export-pdf=$@ $(<)

# Rules for FIG files (xfig)
# Create combined pdf/latex figures from .fig file
$(XFIG_FIGS): $(FIGS_DIR)/%.pdf_t: $(FIGS_ORIG_DIR)/%.fig
	echo $*
	fig2dev -L pdftex -p dummy $(FIGS_ORIG_DIR)/$*.fig > $(FIGS_DIR)/$*.pdf
	fig2dev -L pdftex_t -p $(FIGS_DIR)/$* $(FIGS_ORIG_DIR)/$*.fig > $(FIGS_DIR)/$*.pdf_t


$(MAIN).bbl : $(MAIN).tex $(BIBFILES)
	pdflatex $(MAIN).tex
	bibtex $(MAIN)
