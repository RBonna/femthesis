
# Description #

LaTeX class for typesetting master's and doctor's thesis, according to the rules of Faculty of Mechanical Engineering of University of Campinas. The current norm is given by the
version, as explained in versioning.

The provided class (femthesis.cls) contains all commands for automatic generation of front and back matter such as titlepage, signatures page etc. At the moment, the class has two additional options to modify its generation: englishthesis and phd (doutorado); which change how automatic generation from meta data is to be done. An example document is available as example.tex, until any actual documentation is done.

## No Portugu\^{e}s? ##

There used to be a Portuguese translation of this README some time ago, however, for the moment, this repo was basically maintained by one guy (me! hello.) and
keeping two versions of the same exact text is tedious and troublesome. Hence, it's gone. You mostly HAVE to know English to handle yourself in academia anyway,
besides the fact that FEM (noticed the title?) basically asks you to have basic commandment of the language.

If you want a TL;DR: I'm lazy to translate it to Portuguese, and you know you cannot argue!

## How to use it ##

Just clone the repo and take the femthesis.cls whenever you need it. That's it. With that said, here goes a few options and their explanations for your convenience.

For Class options, you have "englishthesis", which starts printing out English titles that
you define, besides switching to English versions of some of the front matter (its automatic, do not worry too much about it).
You also have the "phd" or "doutorado" options, which change some of the printed front matter like the former option to reflect the differences between a masters and a phd thesis (such as having 5 signature places instead of 3).

Otherwise everything should work just like a normal LaTeX document, except that most of
the common commands were prepended with a "make", e.g. \maketitlepage, so that it can
coexist with the usual LaTeX commands of the same name. To get a feel of all these little
time savers, take a look at the example; most, if not all, of them will be shown there.
If you know a little bit of LaTeX (or TeX, you damn guru), you can even open femthesis.cls
it self and learn from there.

## Helping out ##

In case you want to help in this project. Remember that the final goal is to promote this class as a foundation for future works. In this context, the lesser the packages, the better.

I have not committed to any style in particular, I so think, but you will notice that I
tried as maximum as possible to keep commands modular in femthesis.cls, and I would
advise you to keep doing so, as anyone that deals with computation would. Also, I know it sucks, by keep using if until hell comes loose to cut the need for extra packages.

## Disclaimer ##

This repository started as a copy of another private repository which the current author was part of.
The admin of the first kept the last repository private and forgotten, with only a very basic README written, although he had great goals and planned to make it public.
So... here you go, it officially started there.

##  Necessary Packages ##

* graphicx (because of the unicamp logo)
* fontenc (luatex) or inpuntec (pdflatex) (switches automatically)
* geometry (you know, margins and stuff)
* fancyhdr (you could say it should be optional, but no one deserves to look at your dense plainly written thesis)
* titlesec (to make the report class manageable)
* babel (only if main language is Portuguese. When changing language, you may need to delete the .log and .aux files before compiling)

## Hall of Fame ##

This lists all defended thesis that survive the bureaucracy of the CPG document review process.

* Rodolfo Jordao, Masters: version 2015-3. After some back and fourth juggling with version 2015-2, the master was finally accepted and version 2015-3 was born.

## Versioning ##

This code should follow a revision scheme to directly represent the b******t FEM
norms that it tries to obey, for instance, if the current norms are from 2013, and this
is the third major revision of this template for said norms,
the version should be: 2013-3.

### Current Version: 2015-3 ###

### Latest big changes ###
* 2018-04-11: Changed the default to print everything continually with no spaces, since that's how the CPG wants it.
* 2018-01-22: Made the catalog page support the professors' names.
* 2017-05-11: Babel and datetime are no more needed, as a simple function for date creation has been made.

#### Authors (Alphabetical order) ####
* Marcos Mendel: Author and publisher of a Thesis template FEM released based on ABNT specs. Had too many pkgs and would not compile.
* Rodolfo Jordão: Initial author and publisher of this public template based on the original one.
* Ricardo Bonna: Current maintainer.
* "Wendell" (thejesusbrr): Owner of the original bitbucket repo this repo is itself a public fork-copy of. He graduated and disappeared. It's ok, I guess.
